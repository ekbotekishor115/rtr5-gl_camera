// Header files
#include<windows.h>
#include<winuser.h>
#include<stdio.h>
#include<stdlib.h>
#include "OGL.h"

// OpenGL header files
#include<GL/glew.h> // THIS MUST BE INCLUDED BEFORE 'gl.h'
#include<GL/gl.h>

// 
#include "vmath.h"
using namespace vmath;

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// OpenGL Libraries
#pragma comment (lib,"glew32.lib")
#pragma comment (lib,"OpenGL32.lib")

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
BOOL gbFullScreen = FALSE;
FILE *gpFile = NULL;
BOOL gbActiveWindow = FALSE;

// Programmable Pipeline related Global variables
GLuint shaderProgramObject;

enum 
{
	KPE_ATTRIBUTE_POSITION = 0,
	KPE_ATTRIBUTE_COLOR,
	KPE_ATTRIBUTE_NORMAL,
	KPE_ATRRIBUTE_TEXTURE0
};

GLuint vao;
GLuint vbo;
GLuint mvpMatrixUniform;

mat4 perspectiveProjectionMatrix;

GLuint numberOfSegmentsUniform;
GLuint numberOfStripsUniform;
GLuint lineColorUniform;
unsigned int uiNumberOfSegments;

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void);
	void uninitialize(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[]=TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iScreenWidth, iScreenHeight;
	int iRetVal = 0;

	// code
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Creation of Log file Failed. Exitting..."), TEXT("File I/O"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log file Successfully Created. \n");
	}

	
	

	// initialization of WNDCLASSEX structure
	wndclass.cbSize = sizeof(WNDCLASSEX);	// was not before 1993
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;	// ya window chya callback function cha addrss
	wndclass.hInstance = hInstance;	// 
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));	//window chya left top cha icon ani task bar cha icon
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));	// Explorer cha icon	//was not before 1993 (newly added)

	// registering above WINDCLASSEX
	RegisterClassEx(&wndclass);

	// Screen dimensions
	iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
			szAppName,
			TEXT("OGLPP Window - Kishor"),
			WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
			(iScreenWidth/4),	//x c-ordinate 
			(iScreenHeight/4),	// y - coordinate
			WIN_WIDTH,	// width
			WIN_HEIGHT,	// height
			NULL,		// parent window handle (desktop window) = HWND_WIMDOW
			NULL,		// Menu handle
			hInstance,
			NULL);

	ghwnd = hwnd;

	// initialize
	iRetVal = initialize();

	if (iRetVal == -1)
	{
		fprintf(gpFile, "ChoosePixelFormat Failed\n");
		uninitialize();
	}

	else if (iRetVal == -2)
	{
		fprintf(gpFile, "SetPixelFormat Failed\n");
		uninitialize();
	}

	else if (iRetVal == -3)
	{
		fprintf(gpFile, "Creating OpenGL Cintext Failed\n");
		uninitialize();
	}

	else if (iRetVal == -4)
	{
		fprintf(gpFile, "Making OpenGL Context as Current Context Failed\n");
		uninitialize();
	}

	else if (iRetVal == -5)
	{
		fprintf(gpFile, "GLEW initialization Failed\n");
		uninitialize();
	}

	else
	{
		fprintf(gpFile, "Initializing Successfull\n");
	}

	// show window
	ShowWindow(hwnd, iCmdShow);

	// forgrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game Loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				// update();
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);
	

	// code
	switch(iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
	
		return(0);
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;
		default:
			break;
		}

		break;
	case WM_KEYDOWN:
        	switch (wParam)
        	{
        	case 27:
        		DestroyWindow(hwnd);
        		break;
			case VK_UP:
				uiNumberOfSegments++;
				if (uiNumberOfSegments >= 30)
				{
					uiNumberOfSegments = 30;
				}
				break;
			case VK_DOWN:
				uiNumberOfSegments--;
				if (uiNumberOfSegments <= 1)
				{
					uiNumberOfSegments = 1;
				}
				break;
        	default:
        		break;
        	}
        	break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	// variable declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	// code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void)
{
	// function declarations
	void resize(int , int );
	void printGLInfo(void);
	void uninitialize(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	// memset((void *) &pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR)); used in LINUX for zeroing out the struct same as zeroMemory(..) in WINDOWS

	// initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; // MS supports OpenGL version 1.0 , not above versions
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // eth kalt ki WM_PAINT ata use honar nahi. ata OpenGL cha pixel painting karel
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; // 24 bits is aslo used

	// get DC
	ghdc = GetDC(ghwnd); // gives neutral DC which can be converted as per need .. It gives DC to fullDESKTOP if NULL is passed as parameter

	// choose Pixel Format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd); // convertes the pixel format to OpenGL of ghdc
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	// set the chosen Pixel Format (sets all 26 parameters of pfd which ChoosePixelFormat has set above)
    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
       	return(-2);


	// create OpenGL Context( now OpenGL rendering handle context is not static , hence above Bridging call creates OpenGL rendering context)
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
		return(-3);

	// make the Rendering context as current context (ata ghdc kade painting nasun i.e tu current context nasun ghrc ha current context ahe (yachyakade painting ahe) ahe.. he sangnyasathi above call )
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return(-4);
	
	// GLEW initialization
	if (glewInit() != GLEW_OK)
	{
		return(-5);
	}

	// Print OpenGL info
	printGLInfo();

	// vertex shader
	const GLchar *vertexShaderSourcecode = 
	"#version 460 core" \
	"\n" \
	"in vec3 a_position;" \
	"void main(void)" \
	"{" \
	"gl_Position = vec4(a_position, 1.0);" \
	"}";

	GLuint vertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	glShaderSource(vertexShaderObject, 1, (const GLchar **)&vertexShaderSourcecode, NULL);
	// for 4th para 'NULL' -> length of array of each source code's string's length (yach array)

	glCompileShader(vertexShaderObject);

	GLint status;
	GLint infoLogLength;
	char *log = NULL;
	glGetShaderiv(vertexShaderObject, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderiv(vertexShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char *)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(vertexShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}
	}

	// Tessellation control shader
	const GLchar *tessellationControlShaderSourcecode = 
	"#version 460 core" \
	"\n" \
	"layout(vertices=8) out;" \
	"uniform int u_numberOfSegments;" \
	"uniform int u_numberOfStrips;" \
	"void main(void)" \
	"{" \
	"gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" \
	"gl_TessLevelOuter[0] = float(u_numberOfStrips);" \
	"gl_TessLevelOuter[1] = float(u_numberOfSegments);" \
	"}";

	GLuint tesselleationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);

	glShaderSource(tesselleationControlShaderObject, 1, (const GLchar **)&tessellationControlShaderSourcecode, NULL);
	// for 4th para 'NULL' -> length of array of each source code's string's length (yach array)

	glCompileShader(tesselleationControlShaderObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;
	glGetShaderiv(tesselleationControlShaderObject, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderiv(tesselleationControlShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char *)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(tesselleationControlShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Tessellation Control Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}
	}

	// Tessellation evaluation shader
	const GLchar *tessellationEvaluationShaderSourcecode = 
	"#version 460 core" \
	"\n" \
	"layout(isolines) in;" \
	"uniform mat4 u_mvpMatrix;" \
	"void main(void)" \
	"{" \
	"vec3 p0 = gl_in[0].gl_Position.xyz;" \
	"vec3 p1 = gl_in[1].gl_Position.xyz;" \
	"vec3 p2 = gl_in[2].gl_Position.xyz;" \
	"vec3 p3 = gl_in[3].gl_Position.xyz;" \
	"float u = gl_TessCoord.x;" \
	"float px = 0.5 * cos()"
	"gl_Position = u_mvpMatrix * vec4(p, 1.0);" \
	"}";

	"vec3 p = p0 * (1 - u) * (1 - u) * (1 - u) + p1 * 3*u * (1 - u) * (1 - u) + p2 * 3*u*u * (1 - u) + p3 *u*u*u;" \
	GLuint tesselleationEvaluationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);

	glShaderSource(tesselleationEvaluationShaderObject, 1, (const GLchar **)&tessellationEvaluationShaderSourcecode, NULL);
	// for 4th para 'NULL' -> length of array of each source code's string's length (yach array)

	glCompileShader(tesselleationEvaluationShaderObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;
	glGetShaderiv(tesselleationEvaluationShaderObject, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderiv(tesselleationEvaluationShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char *)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(tesselleationEvaluationShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Tessellation Evaluation Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}
	}

	// Fragment Shader
	const GLchar *fragmentShaderSourcecode = 
	"#version 460 core" \
	"\n" \
	"uniform vec4 u_lineColor;" \
	"out vec4 FragColor;" \
	"void main(void)" \
	"{" \
	"FragColor = u_lineColor;" \
	"}";

	GLuint fragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(fragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourcecode, NULL);

	glCompileShader(fragmentShaderObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;
	glGetShaderiv(fragmentShaderObject, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetShaderiv(fragmentShaderObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char *)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(fragmentShaderObject, infoLogLength, &written, log);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}
	}

	// shader program object
	shaderProgramObject = glCreateProgram();

	glAttachShader(shaderProgramObject, vertexShaderObject);
	glAttachShader(shaderProgramObject, tesselleationControlShaderObject);
	glAttachShader(shaderProgramObject, tesselleationEvaluationShaderObject);
	glAttachShader(shaderProgramObject, fragmentShaderObject);

	// prelinked binding
	// AMC_ATTRIB_POSITION la a_position map kato ani hech AMC_ATTRIB_POSITION glVertexAttribPointer() cha pahila parameter ahe 
	// mhnjech kuthe data cha array thevnar ahot to apn shader madhunch map karun gheto
	glBindAttribLocation(shaderProgramObject, KPE_ATTRIBUTE_POSITION, "a_position");

	glLinkProgram(shaderProgramObject);

	status = 0;
	infoLogLength = 0;
	log = NULL;
	glGetProgramiv(shaderProgramObject, GL_LINK_STATUS, &status);
	if (status == GL_FALSE)
	{
		glGetProgramiv(shaderProgramObject, GL_INFO_LOG_LENGTH, &infoLogLength);
		if (infoLogLength > 0)
		{
			log = (char *)malloc(infoLogLength);
			if (log != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(shaderProgramObject, infoLogLength, &written, log);
				fprintf(gpFile, "Shader Program Link Log : %s\n", log);
				free(log);
				uninitialize();
			}
		}
	}

	//
	mvpMatrixUniform = glGetUniformLocation(shaderProgramObject, "u_mvpMatrix");
	numberOfSegmentsUniform = glGetUniformLocation(shaderProgramObject, "u_numberOfSegments");
	numberOfStripsUniform = glGetUniformLocation(shaderProgramObject, "u_numberOfStrips");
	lineColorUniform = glGetUniformLocation(shaderProgramObject, "u_lineColor");

	// declaration vertex data arrays
	const GLfloat vertices[] = 
	{
		// front
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,

		// right
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,

		// back
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,

		// left
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f

	};

	// vao and vbo related code
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao); // vao madhe apn vbo chya khalchya 5 steps record kelya jatat , jya Display() madhe vaparavya lagtat
	// recording started
	glGenBuffers(1, &vbo); // 1st para = no of buffers, 2nd para = address from GPU
	glBindBuffer(GL_ARRAY_BUFFER, vbo); // 1st para = target (type of buffer to which vbo is to be bind), 2nd para = vbo

	// 1st para = data bharnyachi jaga, 2nd data = sizeof data, 3rd para = actual data, 4th para = lagich data bhar, dynamic nko
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// vbo madhe target through gelela data OpenGL kasa pahat asel mhnje he function
	glVertexAttribPointer(KPE_ATTRIBUTE_POSITION, // shader madhe data 0th position la map kar
							3, // buffer madhla array  ch data ha 3 ch ek set asa karun pahaycha
							GL_FLOAT, // tya array madhlya data cha data type in GPU contest
							GL_FALSE, // ha true asel tr data (1 t0 -1) ya range madhe dyava lagto .i.e. normalized data
							0, // dhanga takaychi garaj nahi karan fakt position cha array ahe (stride)
							NULL); // dhanga astil tr data kuthay he sangaych ast i.e. data Interleaved asel tr 

	glEnableVertexAttribArray(KPE_ATTRIBUTE_POSITION); //shader madhli 0th position enable karne, mhnjech tith map kelela array enable karne

	glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind vbo
	glBindVertexArray(0); // unbind vao

	// Depth related changes
	glClearDepth(1.0f); 
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// 
	// clear the screen using Blue Color (window kontya color ni paint karaychi te eth sangitl jat pn actually paint kel jat nahi)
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	uiNumberOfSegments = 1; // bcoz we want to draw atleast one line(Geometry)

	perspectiveProjectionMatrix = mat4::identity();

	// warm-up resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}

void printGLInfo(void)
{
	// local variable declarations
	GLint numExtensions = 0;

	// code
	fprintf(gpFile, "OpenGL Vendor : %s\n", glGetString(GL_VENDOR));
	fprintf(gpFile, "OpenGL Renderer : %s\n", glGetString(GL_RENDERER));
	fprintf(gpFile, "OpenGL version : %s\n", glGetString(GL_VERSION));
	fprintf(gpFile, "GLSL version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);

	fprintf(gpFile, "Number Of Supported Extension : %d\n", numExtensions);

	for (int i = 0; i < numExtensions; i++)
	{
		fprintf(gpFile, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	
}

void resize(int width, int height)
{
	// code
	if (height == 0)
		height = 1;	// to avoid divided 0(Illeegal Instruction) in future cause

	glViewport(0, 0, width, height); // screen cha konta bhag dakhvu... sadhya sagli screen dakhvli jat ahe pn he game need nusar customize karta yet.. eth width ai height he GL_SIZEI ya type che ahet ...resize la yetana te int ya type madhe yetat

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/ (GLfloat)height, 0.1f, 100.0f);
}

void display(void) 
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // initialize madhe sangitlele glClearColor che painting actually eth hot.. Frame buffer == COLOR_BUFFER -> true

	// use the shaderProgramObject
	glUseProgram(shaderProgramObject);

	// transformations
	mat4 translationMatrix = mat4::identity();
	mat4 modelViewMatrix = mat4::identity();
	mat4 modelViewProjectionMatrix = mat4::identity();

	translationMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
	modelViewMatrix = translationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;
	
	glUniformMatrix4fv(mvpMatrixUniform, 1, GL_FALSE, modelViewProjectionMatrix);
	glUniform1i(numberOfSegmentsUniform, uiNumberOfSegments);
	TCHAR str[255];
	wsprintf(str, TEXT("OGLPP Window - Kishor || Number Of Segments = %d"), uiNumberOfSegments);
	SetWindowText(ghwnd, str);

	glUniform1i(numberOfStripsUniform, 1);
	glUniform4fv(lineColorUniform, 1, vmath::vec4(1.0f ,1.0f, 0.0f, 1.0f));

	glBindVertexArray(vao);
	// Game coding here
	glPatchParameteri(GL_PATCH_VERTICES, 8);
	glDrawArrays(GL_PATCHES, 0, 8);

	glBindVertexArray(0);

	// unuse the shaderProgramObject
	glUseProgram(0);
	SwapBuffers(ghdc); // 
}


void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void); 

	// code
	if (gbFullScreen)
	{
		ToggleFullScreen();
	}

	// deleting & uninitialization of vbo
	if (vbo)
	{
		glDeleteBuffers(1, &vbo);
		vbo = 0;
	}

	// deletion and uninitialization of vao
	if (vao)
	{
		glDeleteVertexArrays(1, &vao);
		vao = 0;
	}

	// shader uninitialization
	if (shaderProgramObject)
	{
		glUseProgram(shaderProgramObject);

		GLsizei numAttachedShaders;
		glGetProgramiv(shaderProgramObject, GL_ATTACHED_SHADERS, &numAttachedShaders);
		GLuint *shaderObjects = NULL;
		shaderObjects = (GLuint *)malloc(numAttachedShaders * sizeof(GLuint));
		glGetAttachedShaders(shaderProgramObject, numAttachedShaders, &numAttachedShaders, shaderObjects);
		for (GLsizei i = 0; i < numAttachedShaders; i++)
		{
			glDetachShader(shaderProgramObject, shaderObjects[i]);
			glDeleteShader(shaderObjects[i]);
			shaderObjects[i] = 0;
		}
		free(shaderObjects);
		shaderObjects = NULL;
		glUseProgram(0);
		glDeleteProgram(shaderProgramObject);
		shaderProgramObject = 0;
	}

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL); // ghrc chi garaj nahi tr tyache hatatun painting che charges kadhun ghe
	}
	
	if (ghrc) // charges kadhlyavr pn ghrc asto , tr tyala null karun taka
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) 
	{
		ReleaseDC(ghwnd, ghdc); // ata Win32 cha ghdc pn lagat nahi mhnun to ghwnd la deun takaycha and NULL karaycha
		ghdc = NULL;
	}

	if (ghwnd)
	{
		DestroyWindow(ghwnd); // sagl window vr draw karun zal mhnun ata ghwnd pn null karun takaycha
		ghwnd = NULL;
	}

	if (gpFile)
        {
        	fprintf(gpFile, "Log file Successfully Closed.\n");
        	fclose(gpFile);
        	gpFile = NULL;
        }

}


