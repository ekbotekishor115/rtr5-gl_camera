#include <stdio.h>
#include <math.h>

#include "vmath.h"
using namespace vmath;

// float determinant(float [][25], float);
// void cofactor(float [][25], float);
// void transpose(float [][25], float [][25], float);

// float determinant(mat4, float);
// // void cofactor(mat4 , float);
// // void transpose(mat4 , mat4 , float);
// mat4 cofactor(mat4, float);
// mat4 transpose(mat4, mat4, float);

int main()
{
    float determinant(mat4, float);
    mat4 cofactor(mat4, float);
    

    float a[25][25], k, d;
    mat4 ab = mat4::identity();
    int i, j;
    printf("Enter the order of the Matrix : ");
    scanf("%f", &k);
    printf("Enter the elements of %.0fX%.0f Matrix : \n", k, k);
    for (i = 0; i < k; i++)
    {
        for (j = 0; j < k; j++)
        {
            scanf("%f", &ab[i][j]);
        }
    }
    //   d = determinant(a, k);
    d = determinant(ab, k);
    if (d == 0)
    {
        printf("\nInverse of Entered Matrix is not possible\n");
    }
    else
    {
        mat4 out = cofactor(ab, k);
        printf("\n\n\nThe inverse of matrix is : \n");

        for (i = 0; i < k; i++)
        {
            for (j = 0; j < k; j++)
            {
                //  printf("\t%f", inverse[i][j]);
                printf("\t%f", out[i][j]);
            }
            printf("\n");
        }
    }
}

/*For calculating Determinant of the Matrix */
// float determinant(float a[25][25], float k)
float determinant(mat4 a, float k)
{
    float s = 1, det = 0, b[25][25];
    mat4 bb = mat4::identity();
    int i, j, m, n, c;
    if (k == 1)
    {
        return (a[0][0]);
    }
    else
    {
        det = 0;
        for (c = 0; c < k; c++)
        {
            m = 0;
            n = 0;
            for (i = 0; i < k; i++)
            {
                for (j = 0; j < k; j++)
                {
                    // b[i][j] = 0;
                    bb[i][j] = 0;
                    if (i != 0 && j != c)
                    {
                        //    b[m][n] = a[i][j];
                        bb[m][n] = a[i][j];
                        if (n < (k - 2))
                            n++;
                        else
                        {
                            n = 0;
                            m++;
                        }
                    }
                }
            }
            //   det = det + s * (a[0][c] * determinant(b, k - 1));
            det = det + s * (a[0][c] * determinant(bb, k - 1));
            s = -1 * s;
        }
    }

    return (det);
}

// void cofactor(float num[25][25], float f)
// void cofactor(mat4 num, float f)
mat4 cofactor(mat4 num, float f)
{
    vmath::mat4 transpose(vmath::mat4 num, vmath::mat4 fac, float r);
    float determinant(vmath::mat4 a, float k);

    //  float b[25][25], fac[25][25];
    mat4 bb = mat4::identity();
    mat4 fac = mat4::identity();
    int p, q, m, n, i, j;
    for (q = 0; q < f; q++)
    {
        for (p = 0; p < f; p++)
        {
            m = 0;
            n = 0;
            for (i = 0; i < f; i++)
            {
                for (j = 0; j < f; j++)
                {
                    if (i != q && j != p)
                    {
                        // b[m][n] = num[i][j];
                        bb[m][n] = num[i][j];
                        if (n < (f - 2))
                            n++;
                        else
                        {
                            n = 0;
                            m++;
                        }
                    }
                }
            }
            //   fac[q][p] = pow(-1, q + p) * determinant(b, f - 1);
            fac[q][p] = pow(-1, q + p) * determinant(bb, f - 1);
        }
    }
    return transpose(num, fac, f);
}
/*Finding transpose of matrix*/
// void transpose(float num[25][25], float fac[25][25], float r)
// void transpose(mat4 num, mat4 fac, float r)
mat4 transpose(mat4 num, mat4 fac, float r)
{
    float determinant(vmath::mat4 a, float k);

    int i, j;
    float b[25][25], inverse[25][25], d;
    mat4 bb = mat4::identity();
    mat4 inv = mat4::identity();

    for (i = 0; i < r; i++)
    {
        for (j = 0; j < r; j++)
        {
            //  b[i][j] = fac[j][i];
            bb[i][j] = fac[j][i];
        }
    }
    d = determinant(num, r);
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < r; j++)
        {
            // inverse[i][j] = b[i][j] / d;
            inv[i][j] = bb[i][j] / d;
        }
    }

    return inv;
    //    printf("\n\n\nThe inverse of matrix is : \n");

    //    for (i = 0;i < r; i++)
    //     {
    //      for (j = 0;j < r; j++)
    //        {
    //         //  printf("\t%f", inverse[i][j]);
    //          printf("\t%f", inv[i][j]);
    //         }
    //     printf("\n");
    //      }
}