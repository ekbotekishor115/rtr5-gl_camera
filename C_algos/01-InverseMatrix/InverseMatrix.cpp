#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
// #include<math.h>

#include "vmath.h"

using namespace vmath;

#define SIZE 10

int main()
{
		 float a[SIZE][SIZE], x[SIZE], ratio;
         mat4 ab = mat4::identity();
		 int i,j,k,n;
		//  clrscr();
		 /* Inputs */
		 /* 1. Reading order of matrix */
		 printf("Enter order of matrix: ");
		 scanf("%d", &n);
		 /* 2. Reading Matrix */
		 printf("Enter coefficients of Matrix:\n");  /// mat4 vala matrix input yenar instead of this fol loop
		 for(i=1;i<=n;i++)
		 {
			  for(j=1;j<=n;j++)
			  {
				   printf("a[%d][%d] = ",i,j);
				//    scanf("%f", &a[i][j]);
				   scanf("%f", &ab[i][j]);
			  }
		 }
		 /* Augmenting Identity Matrix of Order n */
		 for(i=1;i<=n;i++)
		 {
			  for(j=1;j<=n;j++)
			  {
				   if(i==j)
				   {
				    	// a[i][j+n] = 1;
				    	ab[i][j+n] = 1;
				   }
				   else
				   {
				    	// a[i][j+n] = 0;
				    	ab[i][j+n] = 0;
				   }
			  }
		 }
		 /* Applying Gauss Jordan Elimination */
		 for(i=1;i<=n;i++)
		 {
			//   if(a[i][i] == 0.0)
			  if(ab[i][i] == 0.0)
			  {
				   printf("Mathematical Error! because ab[i][i] = %f at i = %d\n", ab[i][i], i);
				   exit(0);
			  }
			  for(j=1;j<=n;j++)
			  {
				   if(i!=j)
				   {
					    ratio = ab[j][i]/ab[i][i];
					    for(k=1;k<=2*n;k++)
					    {
					     	ab[j][k] = ab[j][k] - ratio*ab[i][k];
					    }
				   }
			  }
		 }
		 /* Row Operation to Make Principal Diagonal to 1 */
		 for(i=1;i<=n;i++)
		 {
			  for(j=n+1;j<=2*n;j++)
			  {
			   	ab[i][j] = ab[i][j]/ab[i][i];
			  }
		 }
		 /* Displaying Inverse Matrix */
		 printf("\nInverse Matrix is:\n");
		 for(i=1;i<=n;i++)
		 {
			  for(j=n+1;j<=2*n;j++)
			  {
			   	printf("%0.3f\t",ab[i][j]);
			  }
			  printf("\n");
		 }
		 getch();
		 return(0);
}
