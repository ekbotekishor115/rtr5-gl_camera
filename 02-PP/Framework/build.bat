md .\src\bin

del .\src\bin\*.obj
del .\src\bin\*.res
del main.exe

cl /c /EHsc /I C:\glew\include ./src/main.cpp ./src/includes/*.cpp

move *.obj .\src\bin\

rc.exe ./src/main.rc

move .\src\*.res .\src\bin\

link .\src\bin\*.obj .\src\bin\*.res /out:main.exe /LIBPATH:C:\glew\lib\Release\x64 user32.lib gdi32.lib /SUBSYSTEM:WINDOWS

main.exe
