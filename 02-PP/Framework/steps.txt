
                        Diffuse Light = ld * kd * (s.n)

1. Calculate eye coordinates by multiplying modelVidew matrix and position attribute vector 
2. Calculate normal matrix by taking 3x3 matrix from upper left of modelView matrix by first inverting and transposing the modelView matrix
3. Transform normals by normalizing the product of normal matrix and normal attribute, this will be our 'n' (Transform normals)
4. Calculate light source vector by normalizing the subtraction of light position vector and eyePosition vector
5. Finally the Diffuse light will be
        ->           ___________________________________
                    |   Diffuse Light = ld * kd * (s.n) |
                    |___________________________________|