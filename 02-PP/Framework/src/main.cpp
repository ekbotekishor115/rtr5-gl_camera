// Header files
#include<windows.h>
#include<winuser.h>
#include<stdio.h>
#include<stdlib.h>
#include "main.h"

// OpenGL header files
#include<GL/glew.h> // THIS MUST BE INCLUDED BEFORE 'gl.h'
#include<GL/gl.h>

#include "./includes/Common.h"
#include "./includes/GLHeadersAndMacros.h"
#include "./includes/GLShaders.h"

#include "./includes/vmath.h"

// effects
#include "./effects/lights/Light.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// OpenGL Libraries
#pragma comment (lib,"glew32.lib")
#pragma comment (lib,"OpenGL32.lib")

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
BOOL gbFullScreen = FALSE;
// FILE *gpFile = NULL;
BOOL gbActiveWindow = FALSE;

// Programmable Pipeline related Global variables
// GLuint shaderProgramObject;

// enum 
// {
// 	KPE_ATTRIBUTE_POSITION = 0,
// 	KPE_ATTRIBUTE_COLOR,
// 	KPE_ATTRIBUTE_TEXCOORD,
// 	KPE_ATTRIBUTE_NORMALS
// };

// GLuint vao_cube;
// GLuint vbo_cube_position;
// GLuint vbo_cube_normal;

// GLfloat angleCube = 0.0f;

// // uniforms
// GLuint modelViewMatrixUniform;
// GLuint projectionMatrixUniform;
// GLuint ldUniform;
// GLuint kdUniform;
// GLuint lightPositionUniform;
// GLuint keyPressedUniform;

// BOOL bLightingEnabled = FALSE;
BOOL bAnimationEnabled = FALSE;
// GLfloat lightDiffuse[] = {1.0f, 1.0f, 1.0f, 1.0f}; // white diffuse light
// GLfloat materialDiffuse[] = {0.5f, 0.5f, 0.5f, 1.0f}; // material diffuse
// GLfloat lightPosition[] = {0.0f, 0.0f, 2.0f, 1.0f};

vmath::mat4 perspectiveProjectionMatrix;

Shader* shader = NULL;
Log* logger = NULL;

// effects
Lights* diffuse = NULL;

// entry-point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void display(void);
	void update(void);
	void uninitialize(void);

	// variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[]=TEXT("MyWindow");
	BOOL bDone = FALSE;
	int iScreenWidth, iScreenHeight;
	int iRetVal = 0;

	// code
	// opening log file
	logger = new Log();
	if (logger->CreateLogFile() == false)
	{
		MessageBox(NULL, TEXT("Framework:: Creation of Log file Failed. Exitting..."), TEXT("File I/O"), MB_OK);
		exit(0);
	}
	else
	{
		logger->PrintLog("Framework:: Log file Successfully Created. \n");
	}

	// initialization of WNDCLASSEX structure
	wndclass.cbSize = sizeof(WNDCLASSEX);	// was not before 1993
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;	// ya window chya callback function cha addrss
	wndclass.hInstance = hInstance;	// 
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));	//window chya left top cha icon ani task bar cha icon
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));	// Explorer cha icon	//was not before 1993 (newly added)

	// registering above WINDCLASSEX
	RegisterClassEx(&wndclass);

	// Screen dimensions
	iScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	iScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	// create the window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
			szAppName,
			TEXT("CAMERA Group : GameEngine"),
			WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
			(iScreenWidth/4),	//x c-ordinate 
			(iScreenHeight/4),	// y - coordinate
			WIN_WIDTH,	// width
			WIN_HEIGHT,	// height
			NULL,		// parent window handle (desktop window) = HWND_WIMDOW
			NULL,		// Menu handle
			hInstance,
			NULL);

	ghwnd = hwnd;

	// initialize
	iRetVal = initialize();

	if (iRetVal == -1)
	{
		logger->PrintLog("ChoosePixelFormat Failed\n");
		uninitialize();
	}

	else if (iRetVal == -2)
	{
		logger->PrintLog("SetPixelFormat Failed\n");
		uninitialize();
	}

	else if (iRetVal == -3)
	{
		logger->PrintLog("Creating OpenGL Cintext Failed\n");
		uninitialize();
	}

	else if (iRetVal == -4)
	{
		logger->PrintLog("Making OpenGL Context as Current Context Failed\n");
		uninitialize();
	}

	else if (iRetVal == -5)
	{
		logger->PrintLog("GLEW initialization Failed\n");
		uninitialize();
	}

	else
	{
		logger->PrintLog("Initializing Successfull:: in Function <%S>, at Line <%d>\n", __FUNCTIONW__, __LINE__);
	}

	// show window
	ShowWindow(hwnd, iCmdShow);

	// forgrounding and focusing the window
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// Game Loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// render the scene
				display();

				// update the scene
				if (bAnimationEnabled == TRUE)
				{
					update();
				}
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);
	

	// code
	switch(iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;

	case WM_ERASEBKGND:
		return(0);
	case WM_CHAR:
		diffuse->KeyBoard(wParam);
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;
		case 'A':
		case 'a':
			if (bAnimationEnabled == FALSE)
				bAnimationEnabled = TRUE;
			else
				bAnimationEnabled = FALSE;
			break;
		
		default:
			break;
		}

		break;
	case WM_KEYDOWN:
        	switch (wParam)
        	{
        	case 27:
        		DestroyWindow(hwnd);
        		break;
        	default:
        		break;
        	}
        	break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void ToggleFullScreen(void)
{
	// variable declarations
	static DWORD dwStyle;
	static WINDOWPLACEMENT wp;
	MONITORINFO mi;

	// code
	wp.length = sizeof(WINDOWPLACEMENT);

	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi.cbSize = sizeof(MONITORINFO);

			if (GetWindowPlacement(ghwnd, &wp) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			ShowCursor(FALSE);
			gbFullScreen = TRUE;
		}
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wp);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
		gbFullScreen = FALSE;
	}
}

int initialize(void)
{
	// function declarations
	void resize(int , int );
	void printGLInfo(void);
	void uninitialize(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	// memset((void *) &pfd, NULL, sizeof(PIXELFORMATDESCRIPTOR)); used in LINUX for zeroing out the struct same as zeroMemory(..) in WINDOWS

	// initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; // MS supports OpenGL version 1.0 , not above versions
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // eth kalt ki WM_PAINT ata use honar nahi. ata OpenGL cha pixel painting karel
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; // 24 bits is aslo used

	// get DC
	ghdc = GetDC(ghwnd); // gives neutral DC which can be converted as per need .. It gives DC to fullDESKTOP if NULL is passed as parameter

	// choose Pixel Format
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd); // convertes the pixel format to OpenGL of ghdc
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	// set the chosen Pixel Format (sets all 26 parameters of pfd which ChoosePixelFormat has set above)
    if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
       	return(-2);


	// create OpenGL Context( now OpenGL rendering handle context is not static , hence above Bridging call creates OpenGL rendering context)
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
		return(-3);

	// make the Rendering context as current context (ata ghdc kade painting nasun i.e tu current context nasun ghrc ha current context ahe (yachyakade painting ahe) ahe.. he sangnyasathi above call )
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
		return(-4);
	
	// GLEW initialization
	if (glewInit() != GLEW_OK)
	{
		return(-5);
	}

	// Initialize Effects / scenes
	diffuse = new Lights();
	if (diffuse->Initialize() == false)
	{
		logger->PrintLog("Failed to initialize Lights :: Function <%S>, Line Number <%d>\n", __FUNCTIONW__, __LINE__);
		return -6;
	}

	// clear the screen using Blue Color (window kontya color ni paint karaychi te eth sangitl jat pn actually paint kel jat nahi)
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Depth related changes
	glClearDepth(1.0f); 
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// 
	perspectiveProjectionMatrix = vmath::mat4::identity();

	// warm-up resize call
	resize(WIN_WIDTH, WIN_HEIGHT);
	return(0);
}


void resize(int width, int height)
{
	// code
	if (height == 0)
		height = 1;	// to avoid divided 0(Illeegal Instruction) in future cause

	glViewport(0, 0, width, height); // screen cha konta bhag dakhvu... sadhya sagli screen dakhvli jat ahe pn he game need nusar customize karta yet.. eth width ai height he GL_SIZEI ya type che ahet ...resize la yetana te int ya type madhe yetat

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width/ (GLfloat)height, 0.1f, 100.0f);
}

void display(void) 
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // initialize madhe sangitlele glClearColor che painting actually eth hot.. Frame buffer == COLOR_BUFFER -> true

	diffuse->Render();
	
	SwapBuffers(ghdc); // 
}

void update(void)
{
	// code
	// angleCube = angleCube + 1.0f;
	// if (angleCube >= 360.0f)
	// 	angleCube = angleCube - 360.0f;
	diffuse->Update();
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void); 

	// code
	if (gbFullScreen)
	{
		ToggleFullScreen();
	}

	// uninitialize light
	diffuse->Uninitialize();

	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL); // ghrc chi garaj nahi tr tyache hatatun painting che charges kadhun ghe
	}
	
	if (ghrc) // charges kadhlyavr pn ghrc asto , tr tyala null karun taka
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	if (ghdc) 
	{
		ReleaseDC(ghwnd, ghdc); // ata Win32 cha ghdc pn lagat nahi mhnun to ghwnd la deun takaycha and NULL karaycha
		ghdc = NULL;
	}

	if (ghwnd)
	{
		DestroyWindow(ghwnd); // sagl window vr draw karun zal mhnun ata ghwnd pn null karun takaycha
		ghwnd = NULL;
	}

	// if (gpFile)
    //     {
    //     	fprintf(gpFile, "Log file Successfully Closed.\n");
    //     	fclose(gpFile);
    //     	gpFile = NULL;
    //     }
	logger->CloseLogFile();

}


