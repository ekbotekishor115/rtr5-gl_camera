#pragma once

#include "../../includes/Common.h"

class Lights : public SceneMaster
{
    public:
        Shader* shader = NULL;
        GLuint shaderProgramObject;
        GLuint modelViewMatrixUniform;
        GLuint projectionMatrixUniform;
        GLuint ldUniform;
        GLuint kdUniform;
        GLuint lightPositionUniform;
        GLuint keyPressedUniform;

        GLuint vao_cube;
        GLuint vbo_cube_position;
        GLuint vbo_cube_normal;
        float angleCube = 0.0f;

        bool bLightingEnabled = false;

        GLfloat lightDiffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f}; // white diffuse light
        GLfloat materialDiffuse[4] = {0.5f, 0.5f, 0.5f, 1.0f}; // material diffuse
        GLfloat lightPosition[4] = {0.0f, 0.0f, 2.0f, 1.0f};

        bool Initialize(void)
        {
            shader = new Shader();

            // create and compile shaders
            // Vertex shader
            GLuint vertexShaderObject = shader->CreateAndCompileShaderObjects("./src/shaders/lights/diffuse.vs", VERTEX);
            
            // Fragment shader
            GLuint fragmentShaderObject = shader->CreateAndCompileShaderObjects("./src/shaders/lights/diffuse.fs", FRAGMENT);

            // Create ShaderProgramObject
            shaderProgramObject = glCreateProgram();

            glAttachShader(shaderProgramObject, vertexShaderObject);
            glAttachShader(shaderProgramObject, fragmentShaderObject);

            // prelinked binding
            // AMC_ATTRIB_POSITION la a_position map kato ani hech AMC_ATTRIB_POSITION glVertexAttribPointer() cha pahila parameter ahe 
            // mhnjech kuthe data cha array thevnar ahot to apn shader madhunch map karun gheto
            glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_POSITION, "aPosition");
            glBindAttribLocation(shaderProgramObject, AMC_ATTRIBUTE_NORMALS, "aNormal");

            // Link shaderProgramObject
            if (shader->LinkShaderProgramObject(shaderProgramObject) == false)
            {
                logger->PrintLog("Failed to Link Shader in Function <%S>, at Line <%d>\n", __FUNCTIONW__, __LINE__);
            }

            // binding uniform locations
            modelViewMatrixUniform = glGetUniformLocation(shaderProgramObject, "uModelViewMatrix");
            projectionMatrixUniform = glGetUniformLocation(shaderProgramObject, "uProjectionMatrix");
            ldUniform = glGetUniformLocation(shaderProgramObject, "uLd");
            kdUniform = glGetUniformLocation(shaderProgramObject, "uKd");
            lightPositionUniform = glGetUniformLocation(shaderProgramObject, "uLightPosition");
            keyPressedUniform = glGetUniformLocation(shaderProgramObject, "uKeyPressed");

            // declaration vertex data arrays
            const GLfloat cubePosition[] =
            {
                
                // front
                1.0f,  1.0f,  1.0f, // top-right of front
                -1.0f,  1.0f,  1.0f, // top-left of front
                -1.0f, -1.0f,  1.0f, // bottom-left of front
                1.0f, -1.0f,  1.0f, // bottom-right of front

                // right
                1.0f,  1.0f, -1.0f, // top-right of right
                1.0f,  1.0f,  1.0f, // top-left of right
                1.0f, -1.0f,  1.0f, // bottom-left of right
                1.0f, -1.0f, -1.0f, // bottom-right of right

                // back
                1.0f,  1.0f, -1.0f, // top-right of back
                -1.0f,  1.0f, -1.0f, // top-left of back
                -1.0f, -1.0f, -1.0f, // bottom-left of back
                1.0f, -1.0f, -1.0f, // bottom-right of back

                // left
                -1.0f,  1.0f,  1.0f, // top-right of left
                -1.0f,  1.0f, -1.0f, // top-left of left
                -1.0f, -1.0f, -1.0f, // bottom-left of left
                -1.0f, -1.0f,  1.0f, // bottom-right of left

                // top
                1.0f,  1.0f, -1.0f, // top-right of top
                -1.0f,  1.0f, -1.0f, // top-left of top
                -1.0f,  1.0f,  1.0f, // bottom-left of top
                1.0f,  1.0f,  1.0f, // bottom-right of top

                // bottom
                1.0f, -1.0f,  1.0f, // top-right of bottom
                -1.0f, -1.0f,  1.0f, // top-left of bottom
                -1.0f, -1.0f, -1.0f, // bottom-left of bottom
                1.0f, -1.0f, -1.0f, // bottom-right of bottom

            };

            GLfloat cubeNormals[] =
            {
                // front surface
                0.0f,  0.0f,  1.0f, // top-right of front
                0.0f,  0.0f,  1.0f, // top-left of front
                0.0f,  0.0f,  1.0f, // bottom-left of front
                0.0f,  0.0f,  1.0f, // bottom-right of front

                // right surface
                1.0f,  0.0f,  0.0f, // top-right of right
                1.0f,  0.0f,  0.0f, // top-left of right
                1.0f,  0.0f,  0.0f, // bottom-left of right
                1.0f,  0.0f,  0.0f, // bottom-right of right

                // back surface
                0.0f,  0.0f, -1.0f, // top-right of back
                0.0f,  0.0f, -1.0f, // top-left of back
                0.0f,  0.0f, -1.0f, // bottom-left of back
                0.0f,  0.0f, -1.0f, // bottom-right of back

                // left surface
                -1.0f,  0.0f,  0.0f, // top-right of left
                -1.0f,  0.0f,  0.0f, // top-left of left
                -1.0f,  0.0f,  0.0f, // bottom-left of left
                -1.0f,  0.0f,  0.0f, // bottom-right of left

                // top surface
                0.0f,  1.0f,  0.0f, // top-right of top
                0.0f,  1.0f,  0.0f, // top-left of top
                0.0f,  1.0f,  0.0f, // bottom-left of top
                0.0f,  1.0f,  0.0f, // bottom-right of top

                // bottom surface
                0.0f, -1.0f,  0.0f, // top-right of bottom
                0.0f, -1.0f,  0.0f, // top-left of bottom
                0.0f, -1.0f,  0.0f, // bottom-left of bottom
                0.0f, -1.0f,  0.0f, // bottom-right of bottom
            };
            // vao and vbo related code
            // vao
            // CUBE
            glGenVertexArrays(1, &vao_cube);
            glBindVertexArray(vao_cube); // vao madhe apn vbo chya khalchya 5 steps record kelya jatat , jya Display() madhe vaparavya lagtat
            
            // recording started
            // vbo for position
            glGenBuffers(1, &vbo_cube_position);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_position);
            // 1st para = data bharnyachi jaga, 2nd data = sizeof data, 3rd para = actual data, 4th para = lagich data bhar, dynamic nko
            glBufferData(GL_ARRAY_BUFFER, sizeof(cubePosition), cubePosition, GL_STATIC_DRAW);
            // vbo madhe target through gelela data OpenGL kasa pahat asel mhnje he function
            glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL); 
            glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION); //shader madhli 0th position enable karne, mhnjech tith map kelela array enable karne
            glBindBuffer(GL_ARRAY_BUFFER, 0); // unbind position vbo

            // vbo for normal
            glGenBuffers(1, &vbo_cube_normal);
            glBindBuffer(GL_ARRAY_BUFFER, vbo_cube_normal);
            glBufferData(GL_ARRAY_BUFFER, sizeof(cubeNormals), cubeNormals, GL_STATIC_DRAW);
            glVertexAttribPointer(AMC_ATTRIBUTE_NORMALS, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMALS);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            glBindVertexArray(0); // unbind vao
            return true;
        }

        void Uninitialize(void)
        {
            if (shader)
            {
                delete shader;
                shader = NULL;
            }
        }

        void Render(void)
        {
            // use the shaderProgramObject
            glUseProgram(shaderProgramObject);

            // transformations
            vmath::mat4 translationMatrix = vmath::mat4::identity();
            vmath::mat4 rotationMatrix_1 = vmath::mat4::identity();
            vmath::mat4 rotationMatrix_2 = vmath::mat4::identity();
            vmath::mat4 rotationMatrix_3 = vmath::mat4::identity();
            vmath::mat4 rotationMatrix = vmath::mat4::identity();
            vmath::mat4 modelViewMatrix = vmath::mat4::identity();

            // "CUBE"
            // transformations
            vmath::mat4 scaleMatrix = vmath::mat4::identity();

            translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
            scaleMatrix = vmath::scale(0.75f, 0.75f, 0.75f);
            rotationMatrix_1 = vmath::rotate(angleCube, 1.0f, 0.0f, 0.0f);
            rotationMatrix_2 = vmath::rotate(angleCube, 0.0f, 1.0f, 0.0f);
            rotationMatrix_3 = vmath::rotate(angleCube, 0.0f, 0.0f, 1.0f);
            rotationMatrix = rotationMatrix_1 * rotationMatrix_2 * rotationMatrix_3;
            modelViewMatrix = translationMatrix * scaleMatrix * rotationMatrix;
            
            glUniformMatrix4fv(modelViewMatrixUniform, 1, GL_FALSE, modelViewMatrix);
            glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, perspectiveProjectionMatrix);

            if (bLightingEnabled == true)
            {
                glUniform1i(keyPressedUniform, 1);
                glUniform3fv(ldUniform, 1, lightDiffuse);
                glUniform3fv(kdUniform, 1, materialDiffuse);
                glUniform4fv(lightPositionUniform, 1, lightPosition);
            }
            else
            {
                glUniform1i(keyPressedUniform, 0);

            }

            glBindVertexArray(vao_cube);

            // Game coding here
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
            glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
            glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
            glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
            glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
            glDrawArrays(GL_TRIANGLE_FAN, 20, 4);

            glBindVertexArray(0);
            // unuse the shaderProgramObject
            glUseProgram(0);
        }

        void Update(void)
        {
            angleCube = angleCube + 1.0f;
            if (angleCube >= 360.0f)
                angleCube = angleCube - 360.0f;
        }

        void KeyBoard(char key)
        {
            switch (key)
            {
            case 'L':
            case 'l':
                bLightingEnabled = !bLightingEnabled;
                break;
            
            default:
                break;
            }
        }
};