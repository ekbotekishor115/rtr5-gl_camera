#version 460 core

in vec3 oDiffuseLight;

uniform int uKeyPressed;

out vec4 FragColor;

void main(void)
{
    if (uKeyPressed == 1)
    {
        FragColor = vec4(oDiffuseLight, 1.0);
    }
    else
    {
        FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }
}
