#version 460 core

in vec4 aPosition;
in vec3 aNormal;

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;
uniform vec3 uLd;
uniform vec3 uKd;
uniform vec4 uLightPosition;
uniform int uKeyPressed;

out vec3 oDiffuseLight;

void main(void)
{
    if (uKeyPressed == 1)
    {
        vec4 eyePosition = uModelViewMatrix * aPosition;
        mat3 normalMatrix = mat3(transpose(inverse(uModelViewMatrix)));
        vec3 n = normalize(normalMatrix * aNormal);
        vec3 s = normalize(vec3(uLightPosition - eyePosition));
        oDiffuseLight = uLd * uKd * (dot(s, n));
    }
    else
    {
        oDiffuseLight = vec3(0.0, 0.0, 0.0);
    }
    gl_Position = uProjectionMatrix * uModelViewMatrix * aPosition;
}
