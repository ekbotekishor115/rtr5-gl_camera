/*
* ***********************************************************************************
*   Author: Pradnya Vijay Gokhale                                                   *
* ***********************************************************************************
*/

#include "GLLog.h"



bool Log::CreateLogFile(void)
{
	//code
	if (FilePtr != NULL)
		return(FALSE);

	if (fopen_s(&FilePtr, "Logger.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Failed To Create Log File \"Logger.txt\" !!! Exitting Now ..."), TEXT("LOG FILE ERROR"), MB_ICONERROR | MB_OK);
		return(FALSE);
	}

	else
	{
		fprintf(FilePtr, "============================================================\n");
		fprintf(FilePtr, "Log File \"Logger.txt\" Has Been Created Successfully !!!\n");
		fprintf(FilePtr, "============================================================\n\n");
	}
	return(TRUE);
}

void Log::PrintGLInfo(void)
{
	// variable declarations
	GLint numExtensions, i;

	// code
	fprintf(FilePtr, "\n");
	fprintf(FilePtr, "OpenGL Related Information : \n");
	fprintf(FilePtr, "===============================\n\n");
	fprintf(FilePtr, "OpenGL Vendor   : %s\n", glGetString(GL_VENDOR));
	fprintf(FilePtr, "OpenGL Renderer : %s\n", glGetString(GL_RENDERER));
	fprintf(FilePtr, "OpenGL Version  : %s\n", glGetString(GL_VERSION));
	fprintf(FilePtr, "GLSL Version    : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtensions);
	fprintf(FilePtr, "Number Of Supportrd Extensions = %d\n", numExtensions);

	for (i = 0; i < numExtensions; i++)
	{
		fprintf(FilePtr, "%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	fprintf(FilePtr, "===============================\n\n");
}

void Log::PrintLog(const char *fmt, ...)
{
	//variable declarations
	va_list arg;
	int ret;

	//code
	if (FilePtr == NULL)
		return;
	va_start(arg, fmt);
	ret = vfprintf(FilePtr, fmt, arg);
	va_end(arg);
}

void Log::CloseLogFile(void)
{
	//code
	if (FilePtr == NULL)
		return;

	fprintf(FilePtr, "============================================================\n");
	fprintf(FilePtr, "Log File \"Logger.txt\" Has Been Closed Successfully !!!\n");
	fprintf(FilePtr, "============================================================\n");

	fclose(FilePtr);
	FilePtr = NULL;
}

