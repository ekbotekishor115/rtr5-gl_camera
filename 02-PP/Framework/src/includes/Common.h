#pragma once

#include "SceneMaster.h"
#include "GLHeadersAndMacros.h"
#include "GLShaders.h"
#include "GLLog.h"

#include "vmath.h"

extern Log* logger;
extern vmath::mat4 perspectiveProjectionMatrix;