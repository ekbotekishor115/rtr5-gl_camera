#pragma once

#include <windows.h> // For BOOL, TEXT(), MessageBox(), etc.
#include <stdio.h> // For fopen_s(), fprintf(), fclose()

#include "GLHeadersAndMacros.h"

class Log
{
    public:
        bool CreateLogFile(void);
        void PrintGLInfo(void);
        void PrintLog(const char *, ...);
        void CloseLogFile(void);

        //global variable declarations
FILE *FilePtr = NULL;
};

