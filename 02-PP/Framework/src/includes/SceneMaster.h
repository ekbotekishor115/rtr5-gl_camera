#pragma once

class SceneMaster
{
    virtual void KeyBoard(char key) = 0;
};
